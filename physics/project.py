from PIL import Image
import math
import random

SIZEX = 400
SIZEY = 400

charges = [[100,200, -1], [300,200, -1], [200, 0, 1]]

img = Image.new('RGB', (SIZEX, SIZEY), color = 'gray')

pixels = img.load()

for x in range(SIZEX):
    for y in range(SIZEY):
        for i in charges:
            
            r = int(((i[0] - x)**2 + (i[1] - y)**2)**0.5)

            red = pixels[x,y][0]
            green = pixels[x,y][1]
            blue = pixels[x,y][2]

            if r != 0:
                green += int(i[2] * 10000/r)
                        
            #green += random.randint(-50,50)
            #red += random.randint(-50,50)
            #blue += random.randint(-50,50)
                        
            pixels[x,y] = (green, green, green)

img.save('art.png')
