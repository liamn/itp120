import java.util.ArrayList;

public class BallotTools {
    public int count(ArrayList<Ballot> thisElection, int excludedCandidate, String winnerorloser) {
        int[] ballotCounts = new int[3];
        int length = thisElection.size();
        for (int i = 0; i < 3; i++) {ballotCounts[i] = 0;}
        for (int i = 0; i < length; i++) {
            int choice = thisElection.get(i).choices[0];

            if (choice == excludedCandidate) {

                    choice = thisElection.get(i).choices[1];

            }
            if (choice > 0) {
                ballotCounts[choice - 1]++;
            }
        }
        int max = 0;
        int maxIndex = 0;
        for (int i = 0; i < 3; i++) {
            if (ballotCounts[i] > max) {
                max = ballotCounts[i];
                maxIndex = i;
            }
        }
        int winner = maxIndex + 1;

        int min = max;
        int minIndex = 0;
        for (int i = 0; i < 3; i++) {
            if ((ballotCounts[i] < min) && (!(i == excludedCandidate - 1))) {
                min = ballotCounts[i];
                minIndex = i;
            }
        }
        int loser = minIndex + 1;

        if (winnerorloser == "loser"){
            return loser;
        }
        return winner;
    }
}
