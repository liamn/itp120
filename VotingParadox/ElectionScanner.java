import java.util.Scanner;
import java.util.ArrayList;

class ElectionScanner {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Ballot> thisElection = new ArrayList<Ballot>();

        int electionNumber = 0;

        while (scanner.hasNextLine()) {
            BallotTools bt = new BallotTools();
            String line = scanner.nextLine();
            String[] splitline = line.split(" ");

            int[] currentchoices = new int[3];
            for (int i = 0; i < 3; i++) {
                currentchoices[i] = Integer.parseInt(splitline[i]);
            }

            if (currentchoices[0] > 0) {
                thisElection.add(new Ballot(currentchoices));
            } else {
                electionNumber++;
                int pluralityWinner = bt.count(thisElection, 0, "winner");

                int exhaustiveLoser = bt.count(thisElection, 0, "loser");
                int exhaustiveWinner = bt.count(thisElection, exhaustiveLoser, "winner");

                int oneTwoPrimaryLoser = bt.count(thisElection, 3, "loser");
                int oneTwoPrimary = bt.count(thisElection, oneTwoPrimaryLoser, "winner");

                int oneThreePrimaryLoser = bt.count(thisElection, 2, "loser");
                int oneThreePrimary = bt.count(thisElection, oneThreePrimaryLoser, "winner");

                int twoThreePrimaryLoser = bt.count(thisElection, 1, "loser");
                int twoThreePrimary = bt.count(thisElection, twoThreePrimaryLoser, "winner");

                System.out.println("-------- ELECTION  " + electionNumber);
                System.out.println(" plurality winner  " + pluralityWinner);
                System.out.println("exhaustive ballot  " + exhaustiveWinner);
                System.out.println("       12 primary  " + oneTwoPrimary);
                System.out.println("       13 primary  " + oneThreePrimary);
                System.out.println("       23 primary  " + twoThreePrimary);
                thisElection.clear();
            }
        }
    }
}
