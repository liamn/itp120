import java.util.Scanner;

class TournamentScanner {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int[] houseCounts = new int[]{0,0,0,0};
        String[] names = new String[]{"Gryfindor", "Ravenclaw", "Slytherin", "Hufflepuff"};

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            
            boolean afterComma = false;
            boolean readingHouseName = false;
            for (char c : line.toCharArray()) {
                outerloop:
                if (!afterComma && c == ',') {
                    afterComma =  true;
                } else if (!readingHouseName && c != ' ') {
                    readingHouseName = true;
                } else if (readingHouseName) {
                    for (int n = 0; n<4; n++) {
                        if (c == names[n].charAt(0)) {
                           houseCounts[n]++;
                        break outerloop;
                        }
                    }
                }
            }
        }

        boolean weGood = true;
        for (int i = 0; i < 4; i++) {
            if (houseCounts[i] < 7) {
                System.out.println(names[i]+ " does not have enough players.");
                weGood = false;
            } else if (houseCounts[i] > 7) {
                System.out.println(names[i]+ " has too many players.");
                weGood = false;
            }
        }
        if (weGood){
            System.out.println("List complete, let’s play quidditch!");
        }
    }
}
