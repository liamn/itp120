import java.io.File;
import javax.sound.midi.*;

public class MidiTest {

    public static void main(String[] args) {
        MidiTest mini = new MidiTest();
        if (args.length < 1) {
            System.out.println("Include a .mid file");
        } else {
            mini.play(args[0]);
        }
    }

    public void play(String midiname) {
        try {
            Sequencer player = MidiSystem.getSequencer();
            player.open();
            Sequence seq = MidiSystem.getSequence(new File(midiname));

            player.setSequence(seq);

            player.start();
        } catch (Exception ex) {ex.printStackTrace();}
    }
}
