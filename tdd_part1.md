# What are the Three Laws of Test-driven Development?
1. Write NO production code except to pass a failing test.
2. Write only enough of a test to demonstrate a failure.
3. Write only enough production code to pass the test.

# Explain the Red -> Green -> Refactor -> Red process.

Uncle Bob describes the cycle as follows:
1. Create a unit tests that fails (Red)
2. Write production code that makes that test pass. (Green)
3. Clean up the mess you just made. (Refactor)


# What are the three characteristics Uncle Bob lists of rotting code? (Note: these were covered in Episode 1).
The three characteristics of rotting code are fragility, rigidity, and inseparability.


# Explain how fear promotes code rot and why the fear exists in the first place. How does TDD help us break this vicious cycle?
Fear makes you not want to touch bad code, which prevents it from being improved, which leads to more fear of revisiting rotting code.
The fear exists in the first place because we know that if we want to make changes to rigid code, we have to change things in multiple places, which takes a lot of time and creates more bugs. 
TDD breaks this cycle by ensuring that code is designed to pass a suite of tests which never rot or become outdated. TDD makes you not afraid to clean the code.


# Uncle Bob mentions FitNesse as an example of a project that uses TDD effectively. What is FitNesse? What does it do?
FitNesse is an open-source project that uses TDD, and whose new versions only have to pass a suite of tests before the developers are read to ship them. Wikipedia says FitNesse is "a web server, a wiki and an automated testing tool for software." It can be used to create and automate tests for software.



# What does Uncle Bob say about a program with a long bug list? What does he say this comes from?
Uncle Bob says a long bug list comes from developers behaving unprofessionally.



# What two other benefits does Uncle Bob say you get from TDD besides a reduction in debugging time?
The code becomes easier to read, and the system becomes more flexible.

