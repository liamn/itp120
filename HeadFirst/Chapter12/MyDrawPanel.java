import java.awt.*;
import javax.swing.*;

class MyDrawPanel extends JPanel {
    public void paintComponent(Graphics g) {
        
        Graphics2D g2d = (Graphics2D) g;

        g2d.setColor(Color.orange);
        g2d.fillRect(20,50,100,100);

        GradientPaint gradient = new GradientPaint(70,70, Color.blue, 150,150, Color.orange);

        Image image = new ImageIcon("tiger.jpg").getImage();
        g2d.drawImage(image, 0, 0, this);

        g2d.setPaint(gradient);
        g2d.fillOval(70,70,100,100);
    }
}
