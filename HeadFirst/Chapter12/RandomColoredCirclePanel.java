import java.awt.*;
import javax.swing.*;

class RandomColoredCirclePanel extends JPanel {
    public void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;

        int r = (int) (Math.random() * 255);
        int gr = (int) (Math.random() * 255);
        int b = (int) (Math.random() * 255);

        Color startColor = new Color(r,gr,b);

        r = (int) (Math.random() * 255);
        gr = (int) (Math.random() * 255);
        b = (int) (Math.random() * 255);

        Color endColor = new Color(r,gr,b);

        GradientPaint gradient = new GradientPaint(70,70,startColor, 150,150, endColor);
        g2d.setPaint(gradient);
        g2d.fillOval(70,70,100,100);
    }
}
