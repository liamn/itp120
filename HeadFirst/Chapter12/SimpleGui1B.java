import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

public class SimpleGui1B implements ActionListener {
    JButton button;
    JButton westbutton;
    JButton southbutton;
    JButton eastbutton;
    JButton northbutton;

    RandomColoredCirclePanel mydrawpanel;   

    public static void main (String[] args) {
        SimpleGui1B gui = new SimpleGui1B();
        gui.go();

    }

    public void go() {
        JFrame frame = new JFrame();
        button = new JButton("click me");

        northbutton = new JButton("NORTH");
        southbutton = new JButton("SOUTH");
        westbutton = new JButton("WEST");
        eastbutton = new JButton("EAST");
        button = new JButton("CENTER");
        mydrawpanel = new RandomColoredCirclePanel();

        button.addActionListener(this);

        frame.getContentPane().add(BorderLayout.CENTER, button);
        frame.getContentPane().add(BorderLayout.SOUTH, southbutton);
        frame.getContentPane().add(BorderLayout.NORTH, northbutton);
        frame.getContentPane().add(BorderLayout.WEST, westbutton);
        frame.getContentPane().add(BorderLayout.EAST, eastbutton);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300,300);
        frame.setVisible(true);
    }

    public void actionPerformed(ActionEvent event) {
        button.setText("I've been clicked");
    }
}
