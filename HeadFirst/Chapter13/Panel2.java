import javax.swing.*;
import java.awt.*;

public class Panel2 {
    public static void main (String[] args) {
        Panel2 gui = new Panel2();
        gui.go();
    }   
    
    public void go() {
        JFrame frame = new JFrame();
        JPanel panel = new JPanel();
        
        JButton button = new JButton("top");
        JButton button2 = new JButton("bottom");
        panel.setBackground(Color.blue);
                
        //panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        
        panel.add(button);
        panel.add(button2);
        frame.setContentPane(panel);
        frame.setSize(200,200);
        frame.setVisible(true);
    }   
} 
