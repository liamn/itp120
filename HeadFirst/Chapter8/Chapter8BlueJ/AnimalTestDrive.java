
/**
 * Write a description of class AnimalTestDrive here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class AnimalTestDrive
{
   public static void main (String[] args) {
       MyAnimalAndRobotList list = new MyAnimalAndRobotList();
       Dog a = new Dog();
       Cat c = new Cat();
       Hippo i = new Hippo();
       RobotDog r = new RobotDog();
       Lion l = new Lion();
       Tiger t = new Tiger();
       Wolf w = new Wolf();
       RobotButler b = new RobotButler();
       list.add(a);
       list.add(c);
       list.add(i);
       list.add(r);
       list.add(l);
       list.add(t);
       list.add(w);
       list.add(b);
       
       list.sayNames();
    }
}
