
/**
 * Write a description of class MyAnimalList here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class MyAnimalAndRobotList
{
    // instance variables - replace the example below with your own
    private Object[] objects = new Object[10]; 
    private int nextIndex = 0;
    
    public void add(Object a)
    {
        if (nextIndex < objects.length) {
            objects[nextIndex] = a;
            System.out.println("Animal or robot added at " + nextIndex);
            nextIndex++;
        }
    }
    
    public void sayNames()
    {
        for (Object o: objects) {
           if (o instanceof Animal) {
               Animal j = (Animal) o;
               j.sayName();
           }
           
           if (o instanceof Robot) {
               Robot j = (Robot) o;
               j.sayName();
           }
        }
    }
}
