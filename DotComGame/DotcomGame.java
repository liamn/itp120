import java.util.ArrayList;
import java.util.Random;

class DotcomGame {
    private ArrayList<Dotcom> dotcoms = new ArrayList<Dotcom>();
    private Random rand = new Random();

    private String[][] grid = new String[7][7];

    public String rows = "ABCDEFG";

    public DotcomGame(ArrayList<Dotcom> dots) {
      dotcoms = dots;
    }

    public DotcomGame() {
        String pos;
        String[] names = {"the U.S.S. Amazon", "the U.S.S. Google", "the U.S.S. Facebook"};
        String[] icons = {"A", "G", "F"};
        String rows = "ABCDEFG";

        setupGrid();

        while (dotcoms.size() < names.length) {
            int r = rand.nextInt(5);
            char dir = (rand.nextInt(2) == 0) ? 'V': 'H';
            pos = rows.substring(r,r+1) + Integer.toString(rand.nextInt(5));
            tryToAddDotcom(new Dotcom(pos, dir, names[dotcoms.size()], icons[dotcoms.size()]));
        }
    }

    boolean tryToAddDotcom(Dotcom newDotcom) {
        for (Dotcom dotcom: dotcoms) {
            if (dotcom.collidesWith(newDotcom)) {
              return false;
            }
        }
        dotcoms.add(newDotcom);
        return true;
    }

    public int numDotcoms() {
        return dotcoms.size();
    }

    public boolean over()
    {
        return dotcoms.isEmpty();
    }

    public String guess(String cell) {

        int row = rows.indexOf(cell.substring(0,1));
        int col = Integer.parseInt(cell.substring(1,2));

        for (int i = 0; i < dotcoms.size(); i++) {
            Dotcom dot = dotcoms.get(i);
            String name = dot.getName();
            if (dot.hit(cell)) {

                grid[row][col] = "█";


                  if (dot.isSunk()) {
                      grid[row][col] = dot.getIcon();
                      dotcoms.remove(i);
                      return "You destroyed " + name + "!";

                }
                return "HIT!";
            }
        }
        grid[row][col] = "x";
        return "MISS!";
    }

    public String render() {
        int r = 0;
        String output = "\n  0   1   2   3   4   5   6\n";
        for (String[] row: grid){
            output += rows.substring(r,r+1) + " ";
            for (String col: row) {
                output += col;
                output += "   ";
            }
            output += "\n\n";
            r++;
        }

        return output;
    }

    public void setupGrid() {
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                grid[i][j] = "-";
            }
        }
    }
}
