import java.util.ArrayList;
import java.util.Arrays;

class Dotcom {
    private String name;
    private String icon;
    private ArrayList<String> cells = new ArrayList<String>();
    private boolean[] hits = new boolean[3];

    Dotcom(String pos, char orientation, String setName, String setIcon) {
        name = setName;
        icon = setIcon;
        cells.add(pos);
        if (orientation == 'V') {
            String column = pos.substring(1,2);
            int rowVal = (int)pos.charAt(0);
            cells.add(String.valueOf((char)(rowVal+1) + column));
            cells.add(String.valueOf((char)(rowVal+2) + column));
        } else {
            String row = pos.substring(0,1);
            int colVal = (int)pos.charAt(1);
            cells.add(row + String.valueOf((char)(colVal+1)));
            cells.add(row + String.valueOf((char)(colVal+2)));
        }
    }

    ArrayList<String> getCells() {
        return cells;
    }

    String getName() {
        return name;
    }

    String getIcon() {
        return icon;
    }

    boolean hit(String cell) {
        int index = cells.indexOf(cell);
        if (index == -1 || hits[index]) {
            return false;
        }
        hits[index] = true;
        return true;
    }

    boolean isSunk() {
        for (boolean hit: hits) {
            if (!hit) {
                return false;
            }
        }
        return true;
    }

    boolean collidesWith(Dotcom dot) {
        for (String cell: cells) {
            if (dot.getCells().indexOf(cell) != -1) {
                return true;
            }
        }
        return false;
    }
}
