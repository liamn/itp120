public class PlayDotcomGame {

    static int guesses = 0;

    public static void main(String[] args) {
        DotcomGame game = new DotcomGame();
        CliReader reader = new CliReader();
        String guess;

        System.out.println("\n! ! ! ! B A T T L E S H I P ! ! ! !\n");
        System.out.println(game.render());
        while (!game.over())
        {
            guesses++;
            System.out.println(
            "\n" + game.guess(reader.input("Enter a guess: ")) + "\n"
            );
            System.out.println(game.render());
        }
        System.out.println("You took " + guesses + " guesses.");
    }
}
