Uncle Bob begins with a short summary of the principle discussed in Episode 9.
What is that principle?

    Single responsibility principle
    - Every class should have a single responsibility over one part of the function.

    Classes have responsibility to their users
    The primary value of software is the flexibility of the software
    Carefully allocate functions to

In the fascinating segments Uncle Bob always starts his videos with, we learn
about the origin of Planck's constant. How did Planck stumble upon this constant
and what lesson of discovery does it reveal?

    He stumbled upon it while trying to find an equation relating color/
    frequency and intensity.
    His discovery helped reveal that energy was quantized, not continuous.
    Without it, the transistor would not have been invented.


Describe the two goals of open-closed principle introduced by Bertrand Meyer in
his classic 1988 book.

    Goal 1: Classes should be "open" for extension.
        Things should be open to being connected to new extensions. Updates
        should take place in the form of extensions.
    Goal 2: Classes should be "closed" to modification.
        If it can't be loaded, it won't rot.
        Don't touch things--just stop using them.


        Has the issue of "moving the problem, not solving it." 19:00


This episode is filled with keepers, but I found this gem particularly worth
noting: The very things that make abstractions useful to us also make them
costly. Explain what Uncle Bob means by this statement.

    Abstractions make code easier to use and more expansive.
    However, abstractions are difficult to modify.



http://students.gctaa.net/~jelkner/CleanCode-E10-OpenClosedPrinciple.mp4
