import java.util.Scanner;

class CaesarCipher {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int key = Integer.parseInt(scanner.nextLine());
        key = key % 26;

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (line == "STOP") {
                break;
            } else {
                char[] chararray = line.toCharArray();
                for (int c = 0; c < chararray.length; c++) {

                    char ch = chararray[c];
                    int shifted = ch;
                    if ((ch >= 'A') && (ch <= 'Z')) {
                        shifted = (ch - 65 + key) % 26;
                        shifted += 65;
                    } else if ((ch >= 'a') && (ch <= 'z')) {
                        shifted = (ch - 97 + key) % 26;
                        shifted += 97;
                    }

                    char newchar = (char) shifted;
                    System.out.print(newchar);
                }
            }
            System.out.print("\n");
        }
    }
}
