//this will probably compile even though it calls methods that the SimpleDotCom class does not have yet.

public class TestSimpleDotCom {
    public static void main(String[] args) {
        SimpleDotCom game = new SimpleDotCom();
        

        int[] locations = {2,3,4};
        game.setLocationCells(locations);


        String userGuess = "2";
        String result = game.checkYourself(userGuess);
        String testResult = "failed";
        if (result.equals("hit")) {
            testResult = "passed";
        }
        System.out.println(testResult);
    }
}
