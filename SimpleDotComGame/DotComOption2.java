class DotComOption2
{
    private int[] locations;
    private int hits = 0;
    private int guesses = 0;

    public DotComOption2()
    {
        int start = (int)(Math.random() * 5);
        this.locations = new int[] {start, start + 1, start + 2};
    }

    public void setLocationCells(int[] setLocations) {
        locations = setLocations;
    }

    public boolean over()
    {
      for (int j=0; j<locations.length; j++)
      {
          // if something is not -1, then the game isn't over.
          if (locations[j] != -1)
          {
              return false;
          }
      }
      return true;
    }

    public String makeMove(String userGuess)
    {
        int guess = Integer.parseInt(userGuess);

        // We're processing a guess, so count it.
        this.guesses++;
        int len = locations.length;

        // iterate through the locations array
        for (int i=0; i<len; i++)
            {
            //if an item is what was guessed, set it to -1
            if (locations[i] == guess)
                {
                locations[i] = -1;
                return "hit";
            }
        }

        return "miss";
    }

    public String displayResults()
    {
        return "You took " + this.guesses + " guesses.";
    }
}
