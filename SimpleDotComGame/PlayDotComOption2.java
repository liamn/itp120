public class PlayDotComOption2 {
    public static void main(String[] args)
    {
        CliReader reader = new CliReader();
        DotComOption2 game = new DotComOption2();

        while (!game.over())
        {
            System.out.println(
                game.makeMove(reader.input("Enter a number: "))
            );
        }
        System.out.println(game.displayResults());
    }
}
